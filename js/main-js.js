// Реалізуйте функцію removeElement(array, item), 
// щоб видалити елемент item з масиву array.

// Наприклад:

// const array = [1, 2, 3, 4, 5, 6, 7];
// removeElement(array, 5 );
// console.log(array);
// // Результат: [1, 2, 3, 4, 6, 7]

let arr = [1, 2, 3, 4, 5, 6, 7];

function removeElement(array, item) {
  let itemIndex = array.indexOf(item);
  if (itemIndex > -1) {
      array.splice(itemIndex, 1);
  }
  return array;
}

console.log(arr);
console.log(removeElement(arr, 5));